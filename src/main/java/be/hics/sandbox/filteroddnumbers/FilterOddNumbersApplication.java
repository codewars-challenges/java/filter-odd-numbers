package be.hics.sandbox.filteroddnumbers;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.Arrays;

@SpringBootApplication
public class FilterOddNumbersApplication {

    public static void main(String[] args) {
        SpringApplication.run(FilterOddNumbersApplication.class, args);
        Integer[] listOfInts = new Integer[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        System.out.println(Kata13December.filterOddNumber(Arrays.asList(listOfInts)));
    }
}
