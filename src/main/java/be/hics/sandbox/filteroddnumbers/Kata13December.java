package be.hics.sandbox.filteroddnumbers;

import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class Kata13December {

    private static Predicate<Integer> isOdd = i -> i % 2 != 0;

    public static List<Integer> filterOddNumber(List<Integer> listOfNumbers) {
        return listOfNumbers.stream().filter(isOdd).collect(Collectors.toList());
    }

}